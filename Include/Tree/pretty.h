#ifndef PRETTY_H
#define PRETTY_H

#include "decl.h"

void pretty_print(struct decl *decl);

#endif
