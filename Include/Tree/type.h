#ifndef TYPE_H
#define TYPE_H

enum typekind {
        TKIND_VOID,
        TKIND_INT,
        TKIND_CHAR,
        TKIND_BOOL,
        TKIND_STRING,
        TKIND_ARRAY,
        TKIND_FUNCT,
};

struct param {
        const char *name;
        struct type *type;
        struct param *next;
};

struct type {
        enum typekind kind;
        struct type *sub;
        struct param *params;
        int array_len;
};

struct param *create_param(const char *name,
                           struct type *type,
                           struct param *next);

struct type *create_type(enum typekind kind,
                         struct type *sub,
                         struct param *params);

struct type *create_array_type(int len, struct type *sub);

#endif
