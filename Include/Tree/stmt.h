#ifndef STMT_H
#define STMT_H

#include "decl.h"
#include "expr.h"
#include "stmt.h"

enum stmt_kind {
        STMTKIND_DECL,
        STMTKIND_EXPR,
        STMTKIND_IF_ELSE,
        STMTKIND_FOR,
        STMTKIND_WHILE,
        STMTKIND_PRINT,
        STMTKIND_RETURN,
        STMTKIND_CODE,
};

struct stmt {
        enum stmt_kind kind;
        struct decl *decl;
        struct expr *init;
        struct expr *main_expr;
        struct expr *final;
        struct stmt *code;
        struct stmt *else_code;
        struct stmt *next;
};

struct stmt *create_stmt(enum stmt_kind kind, struct decl *decl,
                         struct expr *init, struct expr *main_expr,
                         struct expr *final, struct stmt *code,
                         struct stmt *else_code, struct stmt *next);

#endif
