#ifndef EXPR_H
#define EXPR_H

enum expr_kind {
        /* Binary */
        EXPRKIND_ADD,
        EXPRKIND_SUB,
        EXPRKIND_MUL,
        EXPRKIND_DIV,
        EXPRKIND_EXP,
        EXPRKIND_ASSIGN,
        // comparisons
        EXPRKIND_EQEQ,
        EXPRKIND_LEQ,
        EXPRKIND_GEQ,
        EXPRKIND_LE,
        EXPRKIND_GR,
        EXPRKIND_BANGEQ,
        // logical
        EXPRKIND_AND,
        EXPRKIND_OR,
        /* Unary */
        EXPRKIND_NOT,
        EXPRKIND_NEGATE,
        /*  */
        EXPRKIND_NAME,
        EXPRKIND_LITERAL_INTEGER,
        EXPRKIND_LITERAL_STRING,
        EXPRKIND_LITERAL_BOOL,
        EXPRKIND_LITERAL_CHAR,
        /*  */
        EXPRKIND_FUNCALL,
        EXPRKIND_ARG,
        EXPRKIND_SUBSCRIPT,

        /* 'left' will hold the current expression, while right holds
         * either the next item in the list, or if there is none, NULL */
        EXPRKIND_LIST,
};

struct expr {
        enum expr_kind kind;

        struct expr *left;
        struct expr *right;

        const char *name;
        int literal_ibc;

        const char *literal_string;
};

struct expr *create_expr(enum expr_kind kind,
                         struct expr *left,
                         struct expr *right);

struct expr *create_expr_name(const char *name);
struct expr *create_expr_literal_integer(int ival);
struct expr *create_expr_literal_string(const char *sval);
struct expr *create_expr_literal_bool(int bval);
struct expr *create_expr_literal_char(char cval);

#endif
