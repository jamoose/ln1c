#ifndef TREE_H
#define TREE_H

#include "type.h"
#include "expr.h"
#include "stmt.h"

struct decl {
        const char *name;
        struct type *type;
        struct expr *value;
        struct stmt *code;
        struct decl *next;
};

struct decl *create_decl(const char *name, struct type *type,
                         struct expr *value, struct stmt *code,
                         struct decl *next);

#endif
