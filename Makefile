# Makefile

CC = cc
CFLAGS = -Wall

b_dir = build

b_inc_dir = build/include

sourcefiles = Src/main.c

fbfiles = Parser/parser.bison Parser/scanner.flex

parserfiles = Parser/scannerutils.c

treeobjs = build/decl.o build/expr.o build/stmt.o build/type.o build/pretty.o

treesrcs = Tree/decl.c Tree/expr.c Tree/stmt.c Tree/type.c Tree/pretty.c

all: ln1c

ln1c: objs parserobjs $(treeobjs)
	$(CC) -o build/ln1c build/main.o build/scanner.o build/scannerutils.o \
	build/parser.o $(treeobjs)

objs: $(sourcefiles) $(b_dir) parserobjs
	$(CC) -c Src/main.c -o build/main.o \
	-I Include -I $(b_inc_dir)

parserobjs: $(fbfiles) $(b_inc_dir)
	flex -o build/scanner.c Parser/scanner.flex
	bison --defines=$(b_inc_dir)/tokens.h --output=build/parser.c \
	Parser/parser.bison
	$(CC) -c build/scanner.c -o build/scanner.o -I build/include \
	-I Include
	$(CC) -c build/parser.c -o build/parser.o -I build/include \
	-I Include
	$(CC) -c Parser/scannerutils.c -o build/scannerutils.o -I Include

$(treeobjs):
	cd Tree; make

$(b_dir):
	mkdir build

$(b_inc_dir):
	mkdir build/include
