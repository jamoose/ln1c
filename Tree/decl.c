#include "Tree/decl.h"

#include <stdlib.h>

struct decl *create_decl(const char *name, struct type *type,
                         struct expr *value, struct stmt *code,
                         struct decl *next)
{
        struct decl *d = malloc(sizeof(struct decl));
        d->name = name;
        d->type = type;
        d->value = value;
        d->code = code;
        d->next = next;

        return d;
}
