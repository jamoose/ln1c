#include "Tree/stmt.h"

#include <stdlib.h>

struct stmt *create_stmt(enum stmt_kind kind, struct decl *decl,
                         struct expr *init, struct expr *main_expr,
                         struct expr *final, struct stmt *code,
                         struct stmt *else_code, struct stmt *next)
{
        struct stmt *s = malloc(sizeof(struct stmt));

        s->kind = kind;
        s->decl = decl;
        s->init = init;
        s->main_expr = main_expr;
        s->final = final;
        s->code = code;
        s->else_code = else_code;
        s->next = next;

        return s;
}
