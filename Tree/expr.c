#include "Tree/expr.h"

#include <stdlib.h>

struct expr *create_expr(enum expr_kind kind,
                         struct expr *left,
                         struct expr *right)
{
        struct expr *e = malloc(sizeof(struct expr));
        e->kind = kind;
        e->left = left;
        e->right = right;

        e->name = NULL;
        e->literal_ibc = 0;

        e->literal_string = NULL;

        return e;
}

struct expr *create_expr_name(const char *name)
{
        struct expr *e = create_expr(EXPRKIND_NAME, NULL, NULL);
        e->name = name;
        return e;
}

struct expr *create_expr_literal_integer(int ival)
{
        struct expr *e = create_expr(EXPRKIND_LITERAL_INTEGER, NULL, NULL);
        e->literal_ibc = ival;
        return e;
}

struct expr *create_expr_literal_string(const char *sval)
{
        struct expr *e = create_expr(EXPRKIND_LITERAL_STRING, NULL, NULL);
        e->literal_string = sval;
        return e;
}

struct expr *create_expr_literal_bool(int bval)
{
        struct expr *e = create_expr(EXPRKIND_LITERAL_BOOL, NULL, NULL);
        e->literal_ibc = bval;
        return e;
}

struct expr *create_expr_literal_char(char cval)
{
        struct expr *e = create_expr(EXPRKIND_LITERAL_CHAR, NULL, NULL);
        e->literal_ibc = cval;
        return e;
}
