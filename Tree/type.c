#include "Tree/type.h"

#include <stdlib.h>

struct param *create_param(const char *name,
                           struct type *type,
                           struct param *next)
{
        struct param *p = malloc(sizeof(struct param));
        p->name = name;
        p->type = type;
        p->next = next;

        return p;
}

struct type *create_type(enum typekind kind,
                         struct type *sub,
                         struct param *params)
{
        struct type *t = malloc(sizeof(struct type));
        t->kind = kind;
        t->sub = sub;
        t->params = params;
        t->array_len = 0;

        return t;
}

struct type *create_array_type(int len, struct type *sub)
{
        struct type *t = create_type(TKIND_ARRAY, sub, NULL);
        t->array_len = len;

        return t;
}
