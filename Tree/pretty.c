/*
** Pretty printer for the Abstract Syntax Tree
** This will reprint the program from the AST.
** J.M.
 */

#include "Tree/decl.h"
#include "Tree/type.h"
#include "Tree/expr.h"
#include "Tree/stmt.h"

#include <stdio.h>

static void print_type(struct type *type)
{
        switch (type->kind) {
        case TKIND_VOID:
                printf("void");
                break;
        case TKIND_INT:
                printf("int");
                break;
        case TKIND_CHAR:
                printf("char");
                break;
        case TKIND_BOOL:
                printf("bool");
                break;
        case TKIND_STRING:
                printf("string");
                break;
        case TKIND_ARRAY:
                printf("(array");
                printf("[%d]", type->array_len);
                printf(" of ");
                print_type(type->sub);
                printf(")");
                break;
        case TKIND_FUNCT:
                printf("function...");
                break;
        default:
                printf("fail");
                break;
        }
}

static void print_expr(struct expr *expr);

/* expr must be of kind EXPRKIND_ARG */
static void print_args(struct expr *expr)
{
        if (!expr)
                return;
        print_expr(expr->left);
        printf(",");
        print_args(expr->right);
}

static void print_list(struct expr *expr)
{
        if (!expr)
                return;

        print_expr(expr->left);
        printf(",");
        print_list(expr->right);
}

#define PRINT_BINARY(op)     \
        printf("(");         \
        print_expr(expr->left);  \
        printf(op);          \
        print_expr(expr->right); \
        printf(")")

#define PRINT_UNARY(op)          \
        printf("(");             \
        printf(op);              \
        print_expr(expr->left);  \
        printf(")")

static void print_expr(struct expr *expr)
{
        switch (expr->kind) {
        case EXPRKIND_ADD:
                PRINT_BINARY("+");
                break;
        case EXPRKIND_SUB:
                PRINT_BINARY("-");
                break;
        case EXPRKIND_MUL:
                PRINT_BINARY("*");
                break;
        case EXPRKIND_DIV:
                PRINT_BINARY("/");
                break;
        case EXPRKIND_EXP:
                PRINT_BINARY("^");
                break;
        case EXPRKIND_ASSIGN:
                PRINT_BINARY("=");
                break;
        case EXPRKIND_EQEQ:
                PRINT_BINARY("==");
                break;
        case EXPRKIND_LEQ:
                PRINT_BINARY("<=");
                break;
        case EXPRKIND_GEQ:
                PRINT_BINARY(">=");
                break;
        case EXPRKIND_LE:
                PRINT_BINARY("<");
                break;
        case EXPRKIND_GR:
                PRINT_BINARY(">");
                break;
        case EXPRKIND_BANGEQ:
                PRINT_BINARY("!=");
                break;
        case EXPRKIND_AND:
                PRINT_BINARY("&&");
                break;
        case EXPRKIND_OR:
                PRINT_BINARY("||");
                break;
        case EXPRKIND_NOT:
                PRINT_UNARY("!");
                break;
        case EXPRKIND_NEGATE:
                PRINT_UNARY("-");
                break;
        case EXPRKIND_NAME:
                printf("%s", expr->name);
                break;
        case EXPRKIND_LITERAL_STRING:
                printf("\"%s\"", expr->literal_string);
                break;
        case EXPRKIND_LITERAL_BOOL:
                if (expr->literal_ibc)
                        printf("true");
                else
                        printf("false");
                break;
        case EXPRKIND_LITERAL_CHAR:
                printf("\'%c\'", expr->literal_ibc);
                break;
        case EXPRKIND_LITERAL_INTEGER:
                printf("%d", expr->literal_ibc);
                break;
        case EXPRKIND_FUNCALL:
                print_expr(expr->left);
                printf("(");
                print_args(expr->right);
                printf(")");
                break;
        case EXPRKIND_SUBSCRIPT:
                print_expr(expr->left);
                printf("[");
                print_expr(expr->right);
                printf("]");
                break;
        case EXPRKIND_LIST:
                printf("{");
                print_list(expr);
                printf("}");
                break;
        default:
                printf("expr");
        }
}

static void print_params(struct param *params)
{
        if (!params)
                return;

        printf("%s: ", params->name);
        print_type(params->type);
        printf(",");

        print_params(params->next);
}

static void print_stmt(struct stmt *code);

static void print_decl(struct decl *decl)
{
        if (!decl)
                return;

        if (decl->type->kind == TKIND_FUNCT) {
                printf("funct %s(", decl->name);
                print_params(decl->type->params);
                printf(")");
                if (!decl->code) {
                        printf(";\n");
                }
                else {
                        printf(" = {\n");
                        print_stmt(decl->code);
                        printf("}\n");
                }
        } else {
                printf("let %s: ", decl->name);
                print_type(decl->type);
                if (decl->value) {
                        printf(" = ");
                        print_expr(decl->value);
                        printf(";\n");
                }
        }

        print_decl(decl->next);
}

static void print_stmt(struct stmt *code)
{
        if (!code)
                return;

        switch (code->kind) {
        case STMTKIND_DECL:
                print_decl(code->decl);
                printf(";\n");
                break;
        case STMTKIND_EXPR:
                print_expr(code->init);
                printf(";\n");
                break;
        case STMTKIND_IF_ELSE:
                printf("if ");
                print_expr(code->init);
                printf(" {\n");
                print_stmt(code->code);
                printf("}\n");
                if (code->else_code) {
                        printf("else {\n");
                        print_stmt(code->else_code);
                        printf("}\n");
                }
                break;
        case STMTKIND_FOR:
                printf("for ");
                print_expr(code->init);
                printf(";");
                print_expr(code->main_expr);
                printf(";");
                print_expr(code->final);
                printf(" {\n");
                print_stmt(code->code);
                printf("}\n");
                break;
        case STMTKIND_WHILE:
                printf("while ");
                print_expr(code->init);
                printf(" {\n");
                print_stmt(code->code);
                printf("}\n");
                break;
        case STMTKIND_PRINT:
                printf("print ");
                print_expr(code->init);
                printf(";\n");
                break;
        case STMTKIND_RETURN:
                printf("return ");
                if (code->init)
                        print_expr(code->init);
                printf(";\n");
                break;
        default:
                printf("def");
        }

        print_stmt(code->next);
}

void pretty_print(struct decl *decl)
{
        /*if (decl->type->kind == TKIND_FUNCT) {
                printf("funct %s(", decl->name);
                print_params(decl->type->params);
                printf(")");
                if (!decl->code) {
                        printf(";");
                }
                else {
                        printf(" = {\n\t");
                        print_stmt(decl->code);
                        printf("}\n");
                }
                }
        else {
                print_decl(decl);
        } */
        print_decl(decl);
}
