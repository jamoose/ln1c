import os
from os import listdir

files = [f for f in listdir("Tests/")]

print(" -- LN-1 COMPILER TESTS -- ")

for n, f in enumerate(files):
    print("\t" + str(n + 1) + " file: " + f)
    print(" ---- STDOUT:")
    ret = os.system("./build/ln1c" + " Tests/" + f)
    print(" ---- RESULT:")
    if ret == 0:
        print("test passed")
    else:
        print("test failed with code " + str(ret))
    print ("\t" + str(n + 1) + " end ---")
