#include "stupid.h"
#include "Parser/scannerutils.h"
#include "tokens.h"
#include "Tree/pretty.h"

#include <stdio.h>
#include <stdlib.h>

/* Forward declarations */
extern FILE *yyin;
extern int yylex();
extern char *yytext;

extern struct decl *parser_result;

extern int yyparse();

extern int get_line();

int main(int argc, char *argv[])
{
        if (argc != 2) {
                fprintf(stderr, "Wrong number of args...\n");
                exit(1);
        }


        yyin = fopen(argv[1], "r");
        if (!yyin) {
                fprintf(stderr, "Could not open test file\n");
                exit(1);
        }

        printf("Hello, ln1c. Version: %s\n", LN1_VERS);

        if (yyparse() == 0) {
                printf("parse succeeded!\n");

                pretty_print(parser_result);
        } else {
                printf("parse failed!\n");
                return 1;
        }
        return 0;
}
