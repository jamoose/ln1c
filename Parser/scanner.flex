%{
#include "tokens.h"
#include "Parser/scannerutils.h"

static int linenum = 1;

int get_line();
%}
DIGIT   [0-9]
LETTER  [a-zA-Z]
%%
(" "|\t)
\n          { ++linenum; }
"#"(.)*
"+"         { return TOKEN_PLUS; }
"-"         { return TOKEN_MINUS; }
"*"         { return TOKEN_STAR; }
"/"         { return TOKEN_SLASH; }
","         { return TOKEN_COMMA; }
"="         { return TOKEN_EQ; }
"!"         { return TOKEN_BANG; }
"=="        { return TOKEN_EQEQ; }
"!="        { return TOKEN_BANGEQ; }
"<="        { return TOKEN_LEQ; }
">="        { return TOKEN_GEQ; }
">"         { return TOKEN_GR; }
"<"         { return TOKEN_LE; }
"&&"        { return TOKEN_AND; }
"||"        { return TOKEN_OR; }
"^"         { return TOKEN_CAR; }
"--"        { return TOKEN_MINUSMINUS; }
"++"        { return TOKEN_PLUSPLUS; }
int         { return TOKEN_KINT; }
char        { return TOKEN_KCHAR; }
bool        { return TOKEN_KBOOL; }
string      { return TOKEN_KSTRING; }
array       { return TOKEN_KARRAY; }
true        { return TOKEN_TRUE; }
false       { return TOKEN_FALSE; }
while       { return TOKEN_KWHILE; }
for         { return TOKEN_KFOR; }
funct       { return TOKEN_KFUNCT; }
if          { return TOKEN_KIF; }
else        { return TOKEN_KELSE; }
print       { return TOKEN_KPRINT; }
return      { return TOKEN_KRETURN; }
void        { return TOKEN_KVOID; }
let         { return TOKEN_KLET; }
of          { return TOKEN_KOF; }
":"         { return TOKEN_COLON; }
";"         { return TOKEN_SEMI; }
"("         { return TOKEN_LPAREN; }
")"         { return TOKEN_RPAREN; }
"{"         { return TOKEN_LBRACE; }
"}"         { return TOKEN_RBRACE; }
"["         { return TOKEN_LBRACKET; }
"]"         { return TOKEN_RBRACKET; }
{DIGIT}+    { return TOKEN_INT; }
'(.|"\\n"|"\\t")'       { string_clean(); return TOKEN_CHAR; }
\"(.)*\"     { string_clean(); return TOKEN_STRING; }
("_"|{LETTER})("_"|{LETTER}|{DIGIT})*     { return TOKEN_IDENT; }
.           { return TOKEN_ERR; }

%%

int yywrap() { return 1; }

int get_line()
{
    return linenum;
}
