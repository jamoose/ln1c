%{
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "Tree/decl.h"
#include "Tree/stmt.h"
#include "Tree/expr.h"
#include "Tree/type.h"

int yylex();

extern char *yytext;

int get_line();

void yyerror(const char *msg)
{
    fprintf(stderr, "%s -> line %d\n", msg, get_line());
}

struct decl *parser_result = NULL;
%}

%union {
       struct decl *decl;
       struct stmt *stmt;
       struct expr *expr;
       struct type *type;
       struct param *params;

       const char *name;
       int literal_n;
};

%type <decl> program decl vardecl fundecl
%type <expr> assign expr assignment assignable subscript primary expr_opt 
logical comparison term factor exponential unary funcall argslist args nextarg
list listnext
%type <name> name
%type <stmt> code stmt elseif_opt else_opt
%type <type> typeid
%type <params> paramlist params nextparam
%type <literal_n> arrsize

%token TOKEN_IDENT
// operators
%token TOKEN_PLUS
%token TOKEN_MINUS
%token TOKEN_STAR
%token TOKEN_SLASH
%token TOKEN_COMMA
%token TOKEN_EQ;
%token TOKEN_BANG;
%token TOKEN_EQEQ;
%token TOKEN_BANGEQ;
%token TOKEN_LEQ;
%token TOKEN_GEQ;
%token TOKEN_GR;
%token TOKEN_LE;
%token TOKEN_AND;
%token TOKEN_OR;
%token TOKEN_CAR;
%token TOKEN_MINUSMINUS;
%token TOKEN_PLUSPLUS;
// keywords
%token TOKEN_KINT
%token TOKEN_KCHAR
%token TOKEN_KBOOL
%token TOKEN_KSTRING
%token TOKEN_KARRAY
%token TOKEN_KWHILE
%token TOKEN_KFOR
%token TOKEN_KFUNCT
%token TOKEN_KIF
%token TOKEN_KELSE
%token TOKEN_KPRINT
%token TOKEN_KRETURN;
%token TOKEN_KVOID;
%token TOKEN_KLET;
%token TOKEN_KOF;
// various symbols
%token TOKEN_COLON
%token TOKEN_SEMI
%token TOKEN_LPAREN
%token TOKEN_RPAREN
%token TOKEN_LBRACE
%token TOKEN_RBRACE
%token TOKEN_LBRACKET
%token TOKEN_RBRACKET
// literals
%token TOKEN_INT
%token TOKEN_TRUE // These two could also be considered keywords
%token TOKEN_FALSE
%token TOKEN_STRING
%token TOKEN_CHAR
// error
%token TOKEN_ERR

%%
program : decl
          { parser_result = $1; }
        ;

decl : vardecl decl
     { $$ = $1; $1->next = $2; }
     | fundecl decl
     { $$ = $1; $1->next = $2; }
     | %empty
     { $$ = NULL; }
     ;

vardecl : TOKEN_KLET name TOKEN_COLON typeid assign TOKEN_SEMI
        { $$ = create_decl($2, $4, $5, NULL, NULL); }
         ;

assign : TOKEN_EQ expr
       { $$ = $2; }
       | %empty
       { $$ = NULL; }
       ;

fundecl : TOKEN_KFUNCT name paramlist TOKEN_COLON typeid TOKEN_EQ TOKEN_LBRACE code TOKEN_RBRACE
        { $$ = create_decl($2, create_type(TKIND_FUNCT, $5, $3),
                           NULL, $8, NULL); }
        | TOKEN_KFUNCT name paramlist TOKEN_COLON typeid TOKEN_SEMI
        { $$ = create_decl($2, create_type(TKIND_FUNCT, $5, $3),
                           NULL, NULL, NULL); }
        ;

paramlist : TOKEN_LPAREN params TOKEN_RPAREN
          { $$ = $2; }
          ;

params : name TOKEN_COLON typeid nextparam
       { $$ = create_param($1, $3, $4); }
       | %empty
       { $$ = NULL; }
       ;

nextparam : TOKEN_COMMA name TOKEN_COLON typeid nextparam
          { $$ = create_param($2, $4, $5); }
          | %empty
          { $$ = NULL; }
          ;

code : stmt code
     { $$ = $1; $1->next = $2; }
     | %empty
     { $$ = NULL; }
     ;

stmt : vardecl
     { $$ = create_stmt(STMTKIND_DECL, $1, 0, 0, 0, 0, 0, 0); }
     | expr TOKEN_SEMI
     { $$ = create_stmt(STMTKIND_EXPR, 0, $1, 0, 0, 0, 0, 0); }
     | TOKEN_KIF expr TOKEN_LBRACE code TOKEN_RBRACE elseif_opt
     { $$ = create_stmt(STMTKIND_IF_ELSE, 0, $2, 0, 0, $4, $6, 0); }
     | TOKEN_KWHILE expr TOKEN_LBRACE code TOKEN_RBRACE
     { $$ = create_stmt(STMTKIND_WHILE, 0, $2, 0, 0, $4, 0, 0); }
     | TOKEN_KFOR expr TOKEN_SEMI expr TOKEN_SEMI expr TOKEN_LBRACE code TOKEN_RBRACE
     { $$ = create_stmt(STMTKIND_FOR, 0, $2, $4, $6, $8, 0, 0); }
     | TOKEN_KPRINT expr TOKEN_SEMI
     { $$ = create_stmt(STMTKIND_PRINT, 0, $2, 0, 0, 0, 0, 0); }
     | TOKEN_KRETURN expr_opt TOKEN_SEMI
     { $$ = create_stmt(STMTKIND_RETURN, 0, $2, 0, 0, 0, 0, 0); }
     ;

elseif_opt : TOKEN_KELSE TOKEN_KIF expr TOKEN_LBRACE code TOKEN_RBRACE elseif_opt
           { $$ = create_stmt(STMTKIND_IF_ELSE, 0, $3, 0, 0, $5, $7, 0); }
           | else_opt
           { $$ = $1; }
           | %empty
           { $$ = NULL; }
           ;

else_opt : TOKEN_KELSE TOKEN_LBRACE code TOKEN_RBRACE
         { $$ = $3; }
         ;

expr_opt : expr
         { $$ = $1; }
	       | %empty
         { $$ = NULL; }
	       ;

expr : assignment
     { $$ = $1; }
     | list
     { $$ = $1; }
     ;

list : TOKEN_LBRACE expr listnext TOKEN_RBRACE
     { $$ = create_expr(EXPRKIND_LIST, $2, $3); }
     ;

listnext : TOKEN_COMMA expr listnext
         { $$ = create_expr(EXPRKIND_LIST, $2, $3); }
         | %empty
         { $$ = NULL; }
         ;

assignment : assignable TOKEN_EQ expr
           { $$ = create_expr(EXPRKIND_ASSIGN, $1, $3); }
           | logical
           { $$ = $1; }
           ;

assignable : name
           { $$ = create_expr_name($1); }
           | subscript
           { $$ = $1; } 
           ;

logical : logical TOKEN_AND comparison
        { $$ = create_expr(EXPRKIND_AND, $1, $3); }
        | logical TOKEN_OR comparison
        { $$ = create_expr(EXPRKIND_OR, $1, $3); }
        | comparison
        { $$ = $1; }
        ;

comparison : term TOKEN_GR term
           { $$ = create_expr(EXPRKIND_GR, $1, $3); }
           | term TOKEN_LE term
           { $$ = create_expr(EXPRKIND_LE, $1, $3); }
           | term TOKEN_EQEQ term
           { $$ = create_expr(EXPRKIND_EQEQ, $1, $3); }
           | term TOKEN_GEQ term
           { $$ = create_expr(EXPRKIND_GEQ, $1, $3); }
           | term TOKEN_LEQ term
           { $$ = create_expr(EXPRKIND_LEQ, $1, $3); }
           | term TOKEN_BANGEQ term
           { $$ = create_expr(EXPRKIND_BANGEQ, $1, $3); }
           | term
           { $$ = $1; }
           ;

term : term TOKEN_PLUS factor
     { $$ = create_expr(EXPRKIND_ADD, $1, $3); }
     | term TOKEN_MINUS factor
     { $$ = create_expr(EXPRKIND_SUB, $1, $3); }
     | factor
     { $$ = $1; }
     ;

factor : factor TOKEN_STAR exponential 
       { $$ = create_expr(EXPRKIND_MUL, $1, $3); }
       | factor TOKEN_SLASH exponential
       { $$ = create_expr(EXPRKIND_DIV, $1, $3); }
       | exponential
       { $$ = $1; }
       ;

exponential : unary TOKEN_CAR exponential
            { $$ = create_expr(EXPRKIND_EXP, $1, $3); }
            | unary
            { $$ = $1; }
            ;

unary : TOKEN_MINUS primary
      { $$ = create_expr(EXPRKIND_NEGATE, $2, NULL); }
      | TOKEN_BANG primary
      { $$ = create_expr(EXPRKIND_NOT, $2, NULL); }
      | primary
      { $$ = $1; }
      ;

primary : TOKEN_INT
        { $$ = create_expr_literal_integer(atoi(yytext)); }
        | TOKEN_STRING
        { $$ = create_expr_literal_string(strdup(yytext)); }
        | TOKEN_CHAR
        { $$ = create_expr_literal_char(*yytext); }
        | TOKEN_TRUE
        { $$ = create_expr_literal_bool(1); }
        | TOKEN_FALSE
        { $$ = create_expr_literal_bool(0); }
        | funcall
        { $$ = $1; }
        | name
        { $$ = create_expr_name($1); }
        | TOKEN_LPAREN expr TOKEN_RPAREN
        { $$ = $2; }
        | subscript
        ;

funcall : name argslist
        { $$ = create_expr(EXPRKIND_FUNCALL,
                           create_expr_name($1), $2); }
        ;

subscript : primary TOKEN_LBRACKET expr TOKEN_RBRACKET
          { $$ = create_expr(EXPRKIND_SUBSCRIPT, $1, $3); }
          ;

argslist : TOKEN_LPAREN args TOKEN_RPAREN
         { $$ = $2; }
         ;

args : expr nextarg
     { $$ = create_expr(EXPRKIND_ARG, $1, $2); }
     | %empty
     { $$ = NULL; }
     ;

nextarg : TOKEN_COMMA expr nextarg
        { $$ = create_expr(EXPRKIND_ARG, $2, $3); }
        | %empty
        { $$ = NULL; }
        ;

name : TOKEN_IDENT
     { $$ = strdup(yytext); }
     ;

typeid : TOKEN_KINT
       { $$ = create_type(TKIND_INT, NULL, NULL); }
       | TOKEN_KCHAR
       { $$ = create_type(TKIND_CHAR, NULL, NULL); }
       | TOKEN_KBOOL
       { $$ = create_type(TKIND_BOOL, NULL, NULL); }
       | TOKEN_KSTRING
       { $$ = create_type(TKIND_STRING, NULL, NULL); }
       | TOKEN_KVOID
       { $$ = create_type(TKIND_VOID, NULL, NULL); }
       | TOKEN_KARRAY TOKEN_LBRACKET arrsize TOKEN_RBRACKET TOKEN_KOF typeid
       { $$ = create_array_type($3, $6); }
       ;
       
arrsize : TOKEN_INT
        { $$ = atoi(yytext); }
        ;
%%
