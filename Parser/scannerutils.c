#include "Parser/scannerutils.h"

#include <string.h>

extern char *yytext;

static void rem(char s[], int n)
{
        int i, j;

        for (i = j = 0; s[i] != '\0'; ++i, ++j) {
                if (i == n)
                        ++j;

                s[i] = s[j];
        }
}

static void clean_escapes(char s[])
{
        int i;
        for (i = 0; i < strlen(s); i++) {
                if (s[i] == '\\') {
                        if (s[i + 1] == 'n') {
                                rem(s, i);
                                s[i] = '\n';
                        } else if (s[i + 1] == 't') {
                                rem(s, i);
                                s[i] = '\t';
                        }
                }
        }
}

void string_clean()
{
        yytext[strlen(yytext) - 1] = '\0';
        ++yytext;
        clean_escapes(yytext);
}
